
import java.io.*;

public class Main {

    public static void main(String[] args){

        // Console console = System.console();
        // String utm = console.readLine("Please enter UTM : ");   
        // System.out.println("You entered : " + utm);

        // PrintWriter writer = new PrintWriter("conversions.txt", "UTF-8");


        Writer writer = null;

        try {
            writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream("conversions.txt"), "utf-8"));

                try {

                    File f = new File("data.txt");
                    BufferedReader b = new BufferedReader(new FileReader(f));
                    String readLine = "";
                    System.out.println("Reading file using Buffered Reader");
        
                    while ((readLine = b.readLine()) != null) {
                        System.out.println(readLine);
                        readLine = parseUTMString(readLine);
                        CoordinateConversion coordinateConversion = new CoordinateConversion();
                        double[] latLon = coordinateConversion.utm2LatLon(readLine);
                        String latLonString = latLon[0]+", "+latLon[1];
                        System.out.println("Lat Lon Result = " + latLonString);
                        writer.write(latLonString);
                        writer.write("\n");
                    }
                    
                } catch (IOException e) {
                    e.printStackTrace();
                }

        } catch (IOException ex) {
            System.out.println("IOException = "+ex.getMessage());
            // Report
        } finally {
            try {
                writer.close();
            } 
            catch (Exception ex) {
                /*ignore*/
            }
        }
    }

    public static String parseUTMString(String utm){
        System.out.println("parseUTMString");
        String parsed;

        int indexOfE = utm.indexOf("E");

        int indexOfFirstLetter = 0;
        for(int i=0;i<utm.length();i++)
        {
            indexOfFirstLetter = i;
            System.out.println("parseUTMString");
            if(Character.isAlphabetic(utm.charAt(i))){
                break;
            }
        }

        System.out.println("indexOfFirstLetter = "+indexOfFirstLetter);
        System.out.println("utm.substring(0, indexOfFirstLetter) = "+utm.substring(0, indexOfFirstLetter));

        parsed = utm.substring(0, indexOfFirstLetter) + " " + utm.substring(indexOfFirstLetter, indexOfE)+ utm.substring(indexOfE+2, utm.length()-2);


        System.out.println("utm.substring(indexOfFirstLetter, indexOfE) = "+"-"+utm.substring(indexOfFirstLetter, indexOfE)+"-");
        System.out.println("utm.substring(indexOfE+2, utm.length()-2) = -"+utm.substring(indexOfE+2, utm.length()-2)+"-");
        // parsed = utm.substring(0, 0) + " " + utm.substring(1, indexOfE-1)+ utm.substring(indexOfE+2, utm.length()-2);
        
        System.out.println("parsed = "+parsed);
        parsed = parsed.replaceAll("m","");
        System.out.println("parsed = "+parsed);
        parsed = parsed.replaceAll("E","");
        System.out.println("parsed = "+parsed);
        parsed = parsed.replaceAll("N","");
        System.out.println("parsed = "+parsed);

        return parsed;
    }
}

