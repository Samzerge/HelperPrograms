
import java.util.*;


public class UTMToLatLong {
    
    public double[] apply(String utm) throws Exception {
        CoordinateConversion conv = new CoordinateConversion();
        double[] latLong = conv.utm2LatLon(utm);
        return latLong;
    }
    
    public double[] apply(Object[] utmIn) throws Exception {
        CoordinateConversion conv = new CoordinateConversion();
        String utm = (String)utmIn[0] + " " + (String)utmIn[1] + " " + (String)utmIn[2] + " " + (String)utmIn[3] ;
        double[] latLong = conv.utm2LatLon(utm);
        return latLong;
    }
    
}